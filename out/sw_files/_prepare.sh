
source _init.sh

if [ "$1" == "cleanup" ]; then
    cd "/media/$SW_MAIN_DIR"
    echo "Cleaning up..."
    sudo rm -r $SW_FILES_DIR_NAME
    sudo rm -r -f *
    return
    echo "THIS SHOULD NOT BE PRINTED!"
fi

if [ "$1" == "sw-artifacts-prepare" ]; then
    PROCEDURE_NAME="Artifacts preparation"
    echo "$(date +'%H:%M:%S, %d.%m.%Y, %A'): $PROCEDURE_NAME started"
    cd "/media/$SW_MAIN_DIR"
    sudo mkdir -p $SW_FILES_DIR_NAME
    cd $SW_FILES_DIR_NAME
    sudo chmod 777 -R .
fi
if [ "$1" == "update-components" ]; then
    PROCEDURE_NAME="Component update"
    echo "$(date +'%H:%M:%S, %d.%m.%Y, %A'): $PROCEDURE_NAME started"
    cd "/$SW_PARENT_DIR/$SW_MAIN_DIR/$SW_FILES_DIR_NAME"
fi

if [ "$1" == "sw-artifacts-prepare" ] || [ "$1" == "update-components" ]; then
    ARTIFACTS_USERNAME="$2"
    ARTIFACTS_PASSWORD="$3"
    curl -O -u $ARTIFACTS_USERNAME:$ARTIFACTS_PASSWORD "$ARTIFACTS_FILES_EXEC"
    if [ ! -z "$ARTIFACTS_FILES_ETC" ]; then
        curl -O -u $ARTIFACTS_USERNAME:$ARTIFACTS_PASSWORD "$ARTIFACTS_FILES_ETC"
    fi
    if [ ! -d "newpan-tools" ]; then
        git clone https://gitlab.com/uit-sfb/newpan-tools.git
    else
        cd "newpan-tools"
        git stash
        git pull
        cd ..
    fi
fi

if [ "$1" == "sw-artifacts-prepare" ]; then
#    if [ ! -d "metapipe" ]; then
#        curl -u $ARTIFACTS_USERNAME:$ARTIFACTS_PASSWORD $ARTIFACTS_FILES_DEPS_ARC | tar xzv -C .
#        # Alternative: tar xvz< <(curl -u $ARTIFACTS_USERNAME:$ARTIFACTS_PASSWORD $ARTIFACTS_FILES_DEPS_ARC)
#        sudo mv ./dist ./metapipe
#    else
#        echo "Deps directory 'metapipe' exists, skipping downloading/unpacking of deps."
#    fi
#    echo "{
#      \"metapipeHome\": \"/$SW_PARENT_DIR/$SW_MAIN_DIR/$SW_FILES_DIR_NAME/metapipe\",
#      \"metapipeTemp\": \"/$SW_PARENT_DIR/$SW_TMP_DIR/metapipe-tmp\"
#}" > metapipe/conf.json
#    cat metapipe/conf.json
#    sudo mkdir .metapipe
#    sudo cp metapipe/conf.json .metapipe

    if [ ! -d "metapipe" ]; then
        mkdir "metapipe"
        java -jar "$SW_EXECUTABLE" packages download-all -d "metapipe"
    else
        echo "Deps directory 'metapipe' exists, skipping downloading/unpacking of deps."
    fi
fi

if [ "$1" == "update-components" ]; then
    cd "/$SW_PARENT_DIR/$SW_MAIN_DIR/$SW_FILES_DIR_NAME"
    if [ ! -d ~/.mc ]; then
        mkdir ~/.mc
    fi
    cp ./mc.json ~/.mc
    rm ~/.mc/config.json
    mv ~/.mc/mc.json ~/.mc/config.json
fi

if [ "$1" == "sw-artifacts-prepare" ] || [ "$1" == "update-components" ]; then
    echo "Waiting..."
    sudo chmod 777 -R .
    echo "$(date +'%H:%M:%S, %d.%m.%Y, %A'): $PROCEDURE_NAME ended"
    return
    echo "THIS SHOULD NOT BE PRINTED!"
fi

# Here we must be already in /$SW_PARENT_DIR/$SW_MAIN_DIR/SW_FILES_DIR_NAME

#sudo ln -s $(pwd)/.metapipe /home/cloud-user/.metapipe

cd ../../$SW_TMP_DIR
sudo mkdir metapipe-tmp
sudo chmod 777 -R .

echo "export METAPIPE_HOME=\"/$SW_PARENT_DIR/$SW_MAIN_DIR/$SW_FILES_DIR_NAME\"" >> ~/.bashrc
echo "source \"/$SW_PARENT_DIR/$SW_MAIN_DIR/$SW_FILES_DIR_NAME/_init.sh\"" >> ~/.bashrc
source ~/.bashrc

# Perl5 module required for Binning
echo y | perl -MCPAN -e'install "LWP::Simple"'

cd "/$SW_PARENT_DIR/$SW_MAIN_DIR/$SW_FILES_DIR_NAME"
./newpan-tools/example/services/authService/auth.sh start
