
source ~/_init.sh

export SW_PARENT_DIR=export
export SW_MAIN_DIR=sw_main
export SW_TMP_DIR=sw_tmp

export SW_FILES_DIR_NAME="sw"

export SW_EXECUTABLE="metapipeCtl-assembly-0.1.3.jar"

export SPARK_JOB_TAG="csc-AAA"

export ARTIFACTS_FILES_EXEC="https://artifacts.sfb.uit.no/jenkins/newpan_tools/85.4/metapipeCtl-assembly-0.1.3.jar"
export ARTIFACTS_FILES_DEPS_ARC="https://artifacts.sfb.uit.no/jenkins/newpan_tools_dependencies/22.1/metapipe-dependencies.tar.gz"
export ARTIFACTS_FILES_ETC=""
