
export SPARK_FILES_DIR=/export/sw_tmp
export CLUSTER_NAME="csc-metapipe-cluster"

export CORES_MASTER=
export RAM_MASTER=

export CORES_PER_SLAVE=
export RAM_PER_SLAVE=

export CORES_PER_EXECUTOR=
export EXECUTORS_PER_SLAVE=$(($CORES_PER_SLAVE / $CORES_PER_EXECUTOR))
export RAM_PER_EXECUTOR=$(($RAM_PER_SLAVE / $EXECUTORS_PER_SLAVE))

export NUM_PARTITIONS=

unset WORKER_HOSTS
declare -a WORKER_HOSTS
while read -r x; do
    temp_str=$(printf "$x" | head -n1 | awk '{print $1;}');
    if [[ $temp_str == *"$CLUSTER_NAME"* ]]; then
      WORKER_HOSTS[${#WORKER_HOSTS[*]}]=$temp_str
    fi
done < <(/usr/sbin/arp -a)
echo "Found connected slaves:"
printf '%s\n' "${WORKER_HOSTS[@]}"

export NUM_SLAVES=${#WORKER_HOSTS[*]}

export PATH=$PATH:$SPARK_FILES_DIR/scala-2.11.12/bin
export SPARK_HOME=$SPARK_FILES_DIR/spark-2.4.0-bin-hadoop2.7
export PATH=$PATH:$SPARK_HOME/bin:$SPARK_HOME/sbin

export SPARK_MASTER="spark://$(hostname):7077"
