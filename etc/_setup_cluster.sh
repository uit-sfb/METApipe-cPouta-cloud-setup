#!/usr/bin/env bash

# Spark Standalone mode

source ~/_init.sh

if [ "$1" == "test" ]; then
    SCRIPT="$2"
    echo -e "
export SPARK_WORKER_INSTANCES=$EXECUTORS_PER_SLAVE
export SPARK_WORKER_MEMORY=$(($RAM_PER_EXECUTOR))g
export SPARK_WORKER_CORES=$CORES_PER_EXECUTOR
" > $SPARK_HOME/conf/spark-env.sh
    . start-all.sh
    SPARK_CONF="\
        --driver-memory $(($RAM_MASTER))G \
        --executor-memory $(($RAM_PER_EXECUTOR))G \
        --conf spark.task.cpus=$CORES_PER_EXECUTOR \
        --conf spark.cores.max=$(($CORES_PER_SLAVE * $NUM_SLAVES))"
    # Spark Standalone, client mode
    $SPARK_HOME/bin/spark-submit \
        --master spark://$(hostname):7077 \
        $SPARK_CONF \
        ~/$SCRIPT
    . stop-all.sh
    exit 1
fi

source ~/_disablePasswordAuth.sh

cd $SPARK_FILES_DIR

sudo yum install wget -y
sudo wget -nv http://downloads.lightbend.com/scala/2.11.12/scala-2.11.12.tgz
sudo tar xf scala-2.11.12.tgz
scala -version
sudo rm scala-2.11.12.tgz

sudo wget -nv https://archive.apache.org/dist/spark/spark-2.4.0/spark-2.4.0-bin-hadoop2.7.tgz
sudo tar xf spark-2.4.0-bin-hadoop2.7.tgz
sudo rm spark-2.4.0-bin-hadoop2.7.tgz

sudo chmod 777 -R $SPARK_FILES_DIR/*

>| $SPARK_HOME/conf/slaves
for name in "${WORKER_HOSTS[@]}"; do
    echo "$name" >> $SPARK_HOME/conf/slaves
    ssh -n -o StrictHostKeyChecking=no cloud-user@$name 'bash -s' < ~/_disablePasswordAuth.sh
done
cat $SPARK_HOME/conf/slaves

#echo "spark.memory.fraction" "0.9" >> $SPARK_HOME/conf/spark-defaults.conf
echo "spark.deploy.defaultCores" "$(($CORES_PER_SLAVE * $NUM_SLAVES))" > $SPARK_HOME/conf/spark-defaults.conf
echo -e "
export SPARK_WORKER_INSTANCES=$EXECUTORS_PER_SLAVE
export SPARK_WORKER_MEMORY=$(($RAM_PER_EXECUTOR))g
export SPARK_WORKER_CORES=$CORES_PER_EXECUTOR
" > $SPARK_HOME/conf/spark-env.sh

. start-all.sh
sudo chmod 777 -R $SPARK_FILES_DIR/*
. stop-all.sh

