
source _init.sh

until echo -e "
export SPARK_WORKER_INSTANCES=$EXECUTORS_PER_SLAVE
export SPARK_WORKER_MEMORY=$(($RAM_PER_EXECUTOR))g
export SPARK_WORKER_CORES=$CORES_PER_EXECUTOR
" > $SPARK_HOME/conf/spark-env.sh; do
    sleep 0.25
done

#java -jar $SW_EXECUTABLE validate

sleep 2

echo "LAUNCHING META-PIPE..."

source $SPARK_HOME/sbin/start-all.sh

#SPARK_CONF="\
#    --driver-cores $CORES_MASTER \
#    --driver-memory $(($RAM_MASTER))G \
#    --executor-cores $CORES_PER_EXECUTOR \
#    --executor-memory $(($RAM_PER_EXECUTOR))G \
#    --conf spark.task.cpus=$CORES_PER_EXECUTOR \
#    --conf spark.cores.max=$(($CORES_PER_EXECUTOR * $NUM_WORKERS))"

SPARK_CONF="\
    --driver-memory $(($RAM_MASTER))G \
    --executor-memory $(($RAM_PER_EXECUTOR))G \
    --conf spark.task.cpus=$CORES_PER_EXECUTOR \
    --conf spark.cores.max=$(($CORES_PER_SLAVE * $NUM_SLAVES))"



# OLD META-pipe command, Spark Standalone, client mode
#$SPARK_HOME/bin/spark-submit \
#    --master spark://$(hostname):7077 \
#    $SPARK_CONF \
#    $SW_EXECUTABLE \
#        execution-manager --executor-id test-executor --num-partitions $NUM_PARTITIONS --num-concurrent-jobs 1 \
#        --config-file .metapipe/conf.json --job-tags $SPARK_JOB_TAG

# META-pipe Classic v2.0 command, Spark Standalone, client mode
#libsDir=$(readlink -e precedenceLibs)
#COMMAND="$SPARK_HOME/bin/spark-submit \
#    --master spark://$(hostname):7077 \
#    --class no.uit.metapipe.workflowTests.Main \
#    --driver-java-options "-Dlogback.configurationFile=logback.xml" \
#    --conf "spark.driver.extraClassPath=$libsDir/*" \
#    --conf "spark.executor.extraClassPath=$libsDir/*" \
#    $SPARK_CONF \
#    $SW_EXECUTABLE test-full-exec -e test-executor -t $SPARK_JOB_TAG \
#    --sysconf-path ./sysconf.json --dist-path ./metapipe \
#    --cache-path /$SW_PARENT_DIR/$SW_TMP_DIR/metapipe-tmp \
#    --admin-login admin --admin-password salvador"
    
# META-pipe RESTRUCTURED v2.0 command, Spark Standalone, client mode
cd "/$SW_PARENT_DIR/$SW_MAIN_DIR/$SW_FILES_DIR_NAME"
./newpan-tools/example/services/authService/auth.sh start
libsDir=$(readlink -e precedenceLibs)
COMMAND="$SPARK_HOME/bin/spark-submit \
    --class no.uit.metapipe.metapipectl.Main \
    --driver-java-options -Dlogback.configurationFile=/$SW_PARENT_DIR/$SW_MAIN_DIR/$SW_FILES_DIR_NAME/logback.xml \
    --conf spark.driver.extraClassPath=$libsDir/* \
    --conf spark.executor.extraClassPath=$libsDir/* \
    $SPARK_CONF \
    $SW_EXECUTABLE start-exec \
    -e ./exec_conf.json \
    -c /$SW_PARENT_DIR/$SW_TMP_DIR/metapipe-tmp \
    -k local -u exec -p password \
    -t $SPARK_JOB_TAG"

echo; echo; echo $COMMAND; echo
$COMMAND

#source $SPARK_HOME/sbin/stop-all.sh
