
META-pipe related contents in this folder.

List of currently required files to run the META-pipe:

_prepare.sh,
_test.sh,
_run.sh - script that launches META-pipe v2.0.

_init.sh - common init variables to be sourced in the scripts above.

metapipe-dependencies.tar.gz - META-pipe dependencies.

workflow-assembly-0.1-SNAPSHOT.jar - META-ppe executable.

conf.json - (for old META-pipe from 2016-2017) META-pipe config file prepared for use on cPouta cluster.
params.json - META-pipe v2.0 config file prepared for use on cPouta cluster.
sysconf.json - META-pipe v2.0 config file prepared for use on cPouta cluster.
logback.xml - META-pipe v2.0 config file for debug output messages prepared for use on cPouta cluster.
