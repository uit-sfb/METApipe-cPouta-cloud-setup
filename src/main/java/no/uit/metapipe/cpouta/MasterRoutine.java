package no.uit.metapipe.cpouta;

class MasterRoutine
{
    private MasterRoutine() { }

    static void masterRoutine(Configuration config)
    {
        System.out.println("Master routine started.\n\n");
        System.out.println("Master routine complete.\n\n");
    }

}
