package no.uit.metapipe.cpouta;

import com.jcraft.jsch.JSch;
import org.jclouds.openstack.nova.v2_0.domain.Server;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

class BastionRoutine
{

    private BastionRoutine() { }

    static void bastionRoutine(String index, JSch ssh, Configuration config, Server master)
    {
        System.out.println("\nBastion routine started with argument '" + index + "'\n\n");
        String execMasterIndex = MainClass.Commands.EXEC_MASTER.getCommand().replace(">", "");
        if(index.equals(MainClass.Commands.UPDATE_COMPONENTS.getCommand()))
        {
            updateToolComponentsOnMaster(ssh, config, master);
        }
        else if(index.equals(MainClass.Commands.CREATE_CLUSTER.getCommand()))
        {
            prepareMaster(ssh, config, master);
            updateToolComponentsOnMaster(ssh, config, master);
            setupCluster(ssh, config, master);
            testCluster(ssh, config, master);
            setupSwOnCluster(ssh, config, master);
//            testSW(ssh, config, master);
        }
        else if(index.startsWith(execMasterIndex))
        {
            executeOnMaster(ssh, config, master, index.replace(execMasterIndex, "").trim());
        }
        else
        {
            if(index.equals(MainClass.Commands.SW_LAUNCH_DEV.getCommand()) || index.equals(MainClass.Commands.SW_STOP_DEV.getCommand()) /*||
                index.equals(MainClass.Commands.TEST_DEV.getCommand())*/ )
            {
                updateToolComponentsOnMaster(ssh, config, master);
            }
            if(index.equals(MainClass.Commands.SW_LAUNCH.getCommand()) || index.equals(MainClass.Commands.SW_LAUNCH_DEV.getCommand()))
            {
                stopSW(ssh, config, master);
                launchSW(ssh, config, master);
            }
            else if(index.equals(MainClass.Commands.SW_STOP.getCommand()) || index.equals(MainClass.Commands.SW_STOP_DEV.getCommand()))
            {
                stopSW(ssh, config, master);
            }
//            else if(index.equals(MainClass.Commands.TEST.getCommand()) || index.equals(MainClass.Commands.TEST_DEV.getCommand()))
//            {
//                stopSW(ssh, config, master);
//                testCluster(ssh, config, master);
//                testSW(ssh, config, master);
//            }
            else
            {
                System.out.println("\nInvalid Bastion routine argument.");
            }
            return;
        }
        System.out.println("\nBastion routine complete.\n");
    }

    private static void prepareMaster(JSch ssh, Configuration config, Server master)
    {
        System.out.println("\nStarted preparing Master...");
        String commands = "sudo yum install -y java-1.8.0-openjdk-devel;";
        commands += 
                "cd " + config.getNfsSwTmpVolumeMount() + ";" + 
                "sudo chmod 777 -R . ; " + 
                "curl -O https://download.docker.com/linux/centos/7/x86_64/stable/Packages/docker-ce-18.09.3-3.el7.x86_64.rpm; " + 
                "curl -O https://download.docker.com/linux/centos/7/x86_64/stable/Packages/docker-ce-cli-18.09.3-3.el7.x86_64.rpm; " + 
                "curl -O https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.4-3.1.el7.x86_64.rpm; " + 
                "sudo yum -y install docker-ce-18.09.3-3.el7.x86_64.rpm docker-ce-cli-18.09.3-3.el7.x86_64.rpm containerd.io-1.2.4-3.1.el7.x86_64.rpm; " + 
                "sudo systemctl start docker; " + 
                "sudo docker run hello-world; " +
                "sudo groupadd docker; " +
                "sudo usermod -aG docker $USER; " +
                "sudo systemctl enable docker; " +
                "sudo curl -L \"https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)\" -o /usr/local/bin/docker-compose; " +
                "sudo chmod +x /usr/local/bin/docker-compose; " +
                "docker-compose --version; ";
        commands += 
                "curl https://bintray.com/sbt/rpm/rpm | sudo tee /etc/yum.repos.d/bintray-sbt-rpm.repo; " +
                "sudo yum install -y sbt; ";
        Utils.sshExecutor(ssh, config.getUserName(),
                Utils.getServerPrivateIp(master, config.getNetworkName()),
                commands);
        System.out.println("Preparation on Master finished.\n");
    }

    private static void updateToolComponentsOnMaster(JSch ssh, Configuration config, Server master)
    {
        System.out.println("\nStarted preparing/updating files on master...");
        File arc = new File("arc.tar");
        String swPath = config.getNfsSwMainVolumeMount() + "/" + config.getSwFilesDirName();
        String commands = "rm -f arc.tar;" + "rm -r -f temp;";
        for(String s : config.getXternFiles().values())
        {
            commands += "rm -r -f " + Utils.getFileNameFromPath(s) + ";";
        }
        Utils.sshExecutor(ssh, config.getUserName(),
                Utils.getServerPrivateIp(master, config.getNetworkName()),
                commands);
        Utils.sshCopier(ssh, config.getUserName(),
                Utils.getServerPrivateIp(master, config.getNetworkName()),
                new String[]{arc.getAbsolutePath()}, "");
        commands = "cd ~; ";
        commands += "tar -xf arc.tar --overwrite 2>&1; ";
        
        commands += "cd " + Utils.getFileNameFromPath(config.getXternFiles().get("sw")) + "; ";
        commands += "cp -R -f ~/sw_files/* " + swPath + "; ";
        commands += "cd " + swPath + "; ";
        
        commands += "sudo chmod 777 -R $(ls | awk '{if($1 != \"metapipe\"){ print $1 }}')";
        Utils.sshExecutor(ssh, config.getUserName(),
                Utils.getServerPrivateIp(master, config.getNetworkName()),
                commands);

        commands =
                "cd " + config.getNfsSwMainVolumeMount() + "/" + config.getSwFilesDirName() + ";" + 
                "source " + config.getSwPrepareScript() + " update-components " +
                config.getSwArtifactsUsername() + " " + config.getSwArtifactsPassword() + " 2>&1;";
        Utils.sshExecutor(ssh, config.getUserName(),
                Utils.getServerPrivateIp(master, config.getNetworkName()), commands);
        
        System.out.println("Required files prepared on Master.\n");
    }

    private static void setupCluster(JSch ssh, Configuration config, Server master)
    {
        System.out.println("\nLaunching Cluster setup on Master...");
        String commands = "source " + Utils.getFileNameFromPath(config.getXternFiles().get("disablePasswordAuth")) + " 2>&1;";
        commands += "source " + Utils.getFileNameFromPath(config.getXternFiles().get("sparkSetupScript")) + " 2>&1;";
        Utils.sshExecutor(ssh, config.getUserName(),
                Utils.getServerPrivateIp(master, config.getNetworkName()), commands);
        System.out.println("Cluster setup on Master complete.\n");
    }

    private static void testCluster(JSch ssh, Configuration config, Server master)
    {
        System.out.println("\nLaunching Cluster test on Master...");
        String commands =
                "source " + Utils.getFileNameFromPath(config.getXternFiles().get("sparkSetupScript")) +
                    " test " + Utils.getFileNameFromPath(config.getXternFiles().get("clusterTestScript")) + " 2>&1;" +
                "sleep 1;";
        Utils.sshExecutor(ssh, config.getUserName(),
                Utils.getServerPrivateIp(master,config.getNetworkName()), commands);
        System.out.println("Cluster test complete.\n");
    }

    private static void setupSwOnCluster(JSch ssh, Configuration config, Server master)
    {
        System.out.println("\nStarted transferring/preparing SW...");
        String commands =
                "cd " + config.getNfsSwMainVolumeMount() + "/" + config.getSwFilesDirName() + ";" +
                "source " + config.getSwPrepareScript() + " 2>&1;";
        Utils.sshExecutor(ssh, config.getUserName(),
                Utils.getServerPrivateIp(master, config.getNetworkName()), commands);
        commands = "docker run --network host --rm -v ~/.mc:/root/.mc minio/mc config host add local " + 
                config.getSwStorageUrl() + " " + config.getSwStorageUsername() + " " + config.getSwStoragePassword() + " 2>&1; ";
        commands += "sudo chmod 755 -R ~/.mc 2>&1; ";
        Utils.sshExecutor(ssh, config.getUserName(),
                Utils.getServerPrivateIp(master, config.getNetworkName()), commands);
        System.out.println("Finished transferring/preparing SW.\n");
    }

//    private static void testSW(JSch ssh, Configuration config, Server master)
//    {
//        System.out.println("\nStarted validating SW...");
//        String commands =
//                "cd " + config.getNfsSwMainVolumeMount() + "/" + config.getSwFilesDirName() + ";" +
//                "source " + config.getSwTestScript() + " 2>&1;";
//        Utils.sshExecutor(ssh, config.getUserName(),
//                Utils.getServerPrivateIp(master,config.getNetworkName()), commands);
//        System.out.println("Finished validating SW.\n");
//    }

    private static void launchSW(JSch ssh, Configuration config, Server master)
    {
        System.out.println("\nLaunching SW...");
        String commands =
                "cd " + config.getNfsSwMainVolumeMount() + "/" + config.getSwFilesDirName() + ";" +
                "source " + config.getSwLaunchScript() + " 2>&1;";
        Utils.sshExecutor(ssh, config.getUserName(),
                Utils.getServerPrivateIp(master,config.getNetworkName()), commands);
    }

    private static void stopSW(JSch ssh, Configuration config, Server master)
    {
        System.out.println("\nStopping SW...");
        String commands =
                "cd " + config.getNfsSwMainVolumeMount() + "/" + config.getSwFilesDirName() + ";" +
                "source " + config.getSwStopScript() + " 2>&1;";
        Utils.sshExecutor(ssh, config.getUserName(),
                Utils.getServerPrivateIp(master,config.getNetworkName()), commands);
    }

    private static void executeOnMaster(JSch ssh, Configuration config, Server master, String tempScriptPath)
    {
        System.out.println("\nExecuting commands on Master...");
        String commands =
                "cd " + config.getNfsSwMainVolumeMount() + "/" + config.getSwFilesDirName() + ";" +
                "source " + config.getSwInitScript() + " 2>&1;" + "cd ~; ";
        try
        {
            commands += new String(
                    Files.readAllBytes(Paths.get(tempScriptPath.replaceFirst("~/", System.getProperty("user.home") + "/"))),
                    "UTF-8");
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return;
        }
        Utils.sshExecutor(ssh, config.getUserName(),
                Utils.getServerPrivateIp(master,config.getNetworkName()), commands);
    }

    static void runMasterRoutine(JSch ssh, Configuration config, Server master)
    {
        System.out.println("Launching Master routine...");
        String commands =
                "java -jar $(pwd)/METApipe-cPouta.jar _master-routine=0;";
        Utils.sshExecutor(ssh, config.getUserName(),
                Utils.getServerPrivateIp(master,config.getNetworkName()), commands);
    }

}
